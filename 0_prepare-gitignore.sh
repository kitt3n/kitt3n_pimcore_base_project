#!/usr/bin/env bash
export $(cat .env | grep -v ^# | xargs)

echo "/.ssh/*" >> ./.gitignore
echo "!/.ssh/.gitkeep" >> ./.gitignore

echo "" >> ./.gitignore
echo "" >> ./.gitignore
echo "/copy/x/*" >> ./.gitignore
echo "!/copy/x/.gitkeep" >> ./.gitignore

echo "/mount/x/app/pim/web/var/*" >> ./.gitignore
echo "!/mount/x/app/pim/web/var/.gitkeep" >> ./.gitignore

echo "/mount/x/app/pim/var/*" >> ./.gitignore
echo "!/mount/x/app/pim/var/.gitkeep" >> ./.gitignore

echo "/mysql_seed/x/*" >> ./.gitignore
echo "!/mysql_seed/x/a.sql" >> ./.gitignore

echo "/mysql_seed_pre/x/*" >> ./.gitignore
echo "!/mysql_seed_pre/x/a.sql" >> ./.gitignore

echo "" >> ./.gitignore
echo "" >> ./.gitignore
echo "/copy/build/*" >> ./.gitignore
echo "!/copy/build/.gitkeep" >> ./.gitignore

echo "/mount/build/app/pim/web/var/*" >> ./.gitignore
echo "!/mount/build/app/pim/web/var/.gitkeep" >> ./.gitignore

echo "/mount/build/app/pim/var/*" >> ./.gitignore
echo "!/mount/build/app/pim/var/.gitkeep" >> ./.gitignore

echo "/mysql_seed/build/*" >> ./.gitignore
echo "!/mysql_seed/build/a.sql" >> ./.gitignore

echo "/mysql_seed_pre/build/*" >> ./.gitignore
echo "!/mysql_seed_pre/build/a.sql" >> ./.gitignore

COUNTER=1
while [  $COUNTER -lt 501 ]; do
    echo "" >> ./.gitignore
    echo "" >> ./.gitignore
    echo "/copy/${COUNTER}/*" >> ./.gitignore
    echo "!/copy/${COUNTER}/.gitkeep" >> ./.gitignore

    echo "/mount/${COUNTER}/app/pim/web/var/*" >> ./.gitignore
    echo "!/mount/${COUNTER}/app/pim/web/var/.gitkeep" >> ./.gitignore

    echo "/mount/${COUNTER}/app/pim/var/*" >> ./.gitignore
    echo "!/mount/${COUNTER}/app/pim/var/.gitkeep" >> ./.gitignore

    echo "/mysql_seed/${COUNTER}/*" >> ./.gitignore
    echo "!/mysql_seed/${COUNTER}/a.sql" >> ./.gitignore

    echo "/mysql_seed_pre/${COUNTER}/*" >> ./.gitignore
    echo "!/mysql_seed_pre/${COUNTER}/a.sql" >> ./.gitignore
    
    let COUNTER=COUNTER+1 
done