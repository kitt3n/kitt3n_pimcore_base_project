#!/usr/bin/env bash
export $(cat .env | grep -v ^# | xargs)

CURRENT_BUILD_VERSION=0
let CURRENT_BUILD_VERSION=${BUILD_VERSION}-1

echo "CURRENT_BUILD_VERSION: ${CURRENT_BUILD_VERSION}"
echo "BUILD_VERSION: ${BUILD_VERSION}"

echo ""

echo "SSH_USER: ${SSH_USER}"
echo "SSH_HOST: ${SSH_HOST}"
echo "SSH_PORT: ${SSH_PORT}"
echo "SSH_BUILD_PATH: ${SSH_BUILD_PATH}"

echo ""

read -r -p "Continue? [Y/n] " response
case "$response" in
    [yY][eE][sS]|[yY])
        
        echo "Continue."

        echo ""
        echo 'Prepare ./mount'
        echo "cp -rp ./mount/x ./mount/${BUILD_VERSION}"
        cp -rp ./mount/x ./mount/${BUILD_VERSION}

        echo ""
        echo "rsync -vaP -e ssh ${SSH_USER}@${SSH_HOST}:${SSH_BUILD_PATH}/mount/${CURRENT_BUILD_VERSION}/app/pim/var/ ./mount/${BUILD_VERSION}/app/pim/var"
        rsync -vaP -e ssh ${SSH_USER}@${SSH_HOST}:${SSH_BUILD_PATH}/mount/${CURRENT_BUILD_VERSION}/app/pim/var/ ./mount/${BUILD_VERSION}/app/pim/var

        echo ""
        echo "rsync -vaP -e ssh ${SSH_USER}@${SSH_HOST}:${SSH_BUILD_PATH}/mount/${CURRENT_BUILD_VERSION}/app/pim/web/var/ ./mount/${BUILD_VERSION}/app/pim/web/var"
        rsync -vaP -e ssh ${SSH_USER}@${SSH_HOST}:${SSH_BUILD_PATH}/mount/${CURRENT_BUILD_VERSION}/app/pim/web/var/ ./mount/${BUILD_VERSION}/app/pim/web/var
        ;;
    *)
        echo "Exit."
        ;;
esac