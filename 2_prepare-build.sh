#!/usr/bin/env bash
export $(cat .env | grep -v ^# | xargs)

CURRENT_BUILD_VERSION=0
let CURRENT_BUILD_VERSION=${BUILD_VERSION}-1

PRE_BUILD_APP_CONTAINER="$(docker-compose -f ./docker-compose.pre.run.yaml --project-name "${BUILD_PROJECT_NAME}_pre_${BUILD_VERSION}" ps -q app)"
PRE_BUILD_MYSQL_CONTAINER="$(docker-compose -f ./docker-compose.pre.run.yaml --project-name "${BUILD_PROJECT_NAME}_pre_${BUILD_VERSION}" ps -q mysqlpim)"

docker ps

echo ""
echo "CURRENT_BUILD_VERSION: ${CURRENT_BUILD_VERSION}"

echo ""
echo "BUILD_VERSION: ${BUILD_VERSION}"

echo ""
echo "PRE_BUILD_APP_CONTAINER: ${PRE_BUILD_APP_CONTAINER}"

echo ""
echo "PRE_BUILD_MYSQL_CONTAINER: ${PRE_BUILD_MYSQL_CONTAINER}"

read -r -p "Continue? [Y/n] " response
case "$response" in
    [yY][eE][sS]|[yY])
        
        echo "Continue."

        echo ""
        echo "1/6"
        echo "mkdir -p ./copy/${CURRENT_BUILD_VERSION} && mv ./copy/build/pim ./copy/${CURRENT_BUILD_VERSION}"
        mkdir -p ./copy/${CURRENT_BUILD_VERSION} && mv ./copy/build/pim ./copy/${CURRENT_BUILD_VERSION}

        echo ""        
        echo "2/6"
        echo "docker cp -a ${PRE_BUILD_APP_CONTAINER}:/app/pim ./copy/build/pim"
        docker cp -a ${PRE_BUILD_APP_CONTAINER}:/app/pim ./copy/build/pim

        echo ""        
        echo "3/6"
        echo 'rm -r ./copy/build/pim/web/var/assets'
        rm -r ./copy/build/pim/web/var/assets

        echo ""
        echo "4/6"
        echo 'rm -r ./copy/build/pim/var/versions'
        rm -r ./copy/build/pim/var/versions

        echo ""
        echo "5/6"
        echo "cp -rp ./mysql_seed/x ./mysql_seed/${BUILD_VERSION}"
        cp -rp ./mysql_seed/x ./mysql_seed/${BUILD_VERSION}

        echo ""
        echo "6/6"
        echo "docker exec ${PRE_BUILD_MYSQL_CONTAINER} /usr/bin/mysqldump -u dev --password=dev dev_pim > ./mysql_seed/${BUILD_VERSION}/dev_pim.sql"
        docker exec ${PRE_BUILD_MYSQL_CONTAINER} /usr/bin/mysqldump -u dev --password=dev dev_pim > ./mysql_seed/${BUILD_VERSION}/dev_pim.sql
        ;;
    *)
        echo "Exit."
        ;;
esac