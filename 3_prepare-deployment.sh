#!/usr/bin/env bash
export $(cat .env | grep -v ^# | xargs)

echo "BUILD_VERSION: ${BUILD_VERSION}"

echo ""

echo "TARGET_SSH_USER: ${TARGET_SSH_USER}"
echo "TARGET_SSH_HOST: ${TARGET_SSH_HOST}"
echo "TARGET_SSH_PORT: ${TARGET_SSH_PORT}"
echo "TARGET_SSH_BUILD_PATH: ${TARGET_SSH_BUILD_PATH}"

echo ""

read -r -p "Continue? [Y/n] " response
case "$response" in
    [yY][eE][sS]|[yY])
        
        echo "Continue."

        read -r -p "Upload database? [Y/n] " response
        case "$response" in
            [yY][eE][sS]|[yY])
                
                echo "Continue."

                echo ""
                echo "rsync -vaP -e ssh ./mysql_seed/${BUILD_VERSION}/ ${TARGET_SSH_USER}@${TARGET_SSH_HOST}:${TARGET_SSH_BUILD_PATH}/mysql_seed/${BUILD_VERSION}"
                rsync -vaP -e ssh ./mysql_seed/${BUILD_VERSION}/ ${TARGET_SSH_USER}@${TARGET_SSH_HOST}:${TARGET_SSH_BUILD_PATH}/mysql_seed/${BUILD_VERSION}
                ;;
            *)
                echo "Not uploading."
                ;;
        esac

        read -r -p "Upload ./mount/${BUILD_VERSION}/app/pim/var/? [Y/n] " response
        case "$response" in
            [yY][eE][sS]|[yY])
                
                echo ""
                echo "rsync -vaP -e ssh ./mount/${BUILD_VERSION}/app/pim/var/ ${TARGET_SSH_USER}@${TARGET_SSH_HOST}:${TARGET_SSH_BUILD_PATH}/mount/${BUILD_VERSION}/app/pim/var"
                rsync -vaP -e ssh ./mount/${BUILD_VERSION}/app/pim/var/ ${TARGET_SSH_USER}@${TARGET_SSH_HOST}:${TARGET_SSH_BUILD_PATH}/mount/${BUILD_VERSION}/app/pim/var
                ;;
            *)
                echo "Not uploading."
                ;;
        esac

        read -r -p "Upload ./mount/${BUILD_VERSION}/app/pim/web/var/? [Y/n] " response
        case "$response" in
            [yY][eE][sS]|[yY])
                
                echo ""
                echo "rsync -vaP -e ssh ./mount/${BUILD_VERSION}/app/pim/web/var/ ${TARGET_SSH_USER}@${TARGET_SSH_HOST}:${TARGET_SSH_BUILD_PATH}/mount/${BUILD_VERSION}/app/pim/web/var"
                rsync -vaP -e ssh ./mount/${BUILD_VERSION}/app/pim/web/var/ ${TARGET_SSH_USER}@${TARGET_SSH_HOST}:${TARGET_SSH_BUILD_PATH}/mount/${BUILD_VERSION}/app/pim/web/var
                ;;
            *)
                echo "Not uploading."
                ;;
        esac
        ;;
    *)
        echo "Exit."
        ;;
esac