#Installation

## Prepare (initial installation)

### Satis

Install satis for private packages

    # Install in packages folder
    $ /opt/plesk/php/7.2/bin/php composer.phar create-project composer/satis --stability=dev --keep-vcs

Put satis.json into packages folder

    {
      "name": "ssf/bam-repository",
      "homepage": "https://packages.xxx.xx",
      "repositories": [
        { "type": "vcs", "url": "git@bitbucket.org:zwei14/c_xxx_kitt3n_pimcore_skeleton.git" },
        { "type": "vcs", "url": "git@bitbucket.org:zwei14/c_xxx_kitt3n_pimcore_custom.git" }
      ],
      "require": {
        "kitt3n/pimcore-skeleton": "dev-custom",
        [...]
      }
    }

 Run satis in packages folder
 
    $ /opt/plesk/php/7.2/bin/php satis/bin/satis build satis.json ./web 
    
Add repository to skeleton composer.json

    "repositories": [
        {
          "type": "composer",
          "url": "https://packages.xxx.xx"
        }
      ],
      
Change version for kitt3n/pimcore-custom

     "require": {
         [...]
         "kitt3n/pimcore-custom": "dev-custom"
       },



## Build and start

    # Build containers
    # This will delete every previously built containers and volumes. Use with care!
    $ make dev-build
    
    # Start containers
    $ make dev-up
    
    # Stop containers
    $ make dev-stop
    
## Install pimcore (local/dev environment)

    $ make dev-root
    
    $ rm -rf /app/pim
    $ php -d memory_limit=-1 composer.phar create-project kitt3n/pimcore-base-skeleton:dev-custom /app/pim --keep-vcs --repository-url=https://packages.xxx.xx
    
    $ /app/pim/vendor/bin/pimcore-install --admin-username sudo --admin-password='<passwd from .env>' --no-interaction
    
    # Enable bundles
    $ /app/pim/bin/console pimcore:bundle:enable Kitt3nPimcoreRestrictionsBundle
    $ /app/pim/bin/console pimcore:bundle:enable Kitt3nPimcoreLayoutsBundle
    $ /app/pim/bin/console pimcore:bundle:enable Kitt3nPimcoreElementsBundle  
    $ /app/pim/bin/console pimcore:bundle:enable Kitt3nPimcoreCustomBundle 
    
    # Install installable bundles
    $ /app/pim/bin/console pimcore:bundle:install Kitt3nPimcoreLayoutsBundle
    
    # Rebuild classes
    $ /app/pim/bin/console pimcore:deployment:classes-rebuild
    
    # Hardcopy assets from bundles
    $ /app/pim/bin/console assets:install
    
## Bundles

    # generate bundle interactively
    $ /app/pim/bin/console pimcore:generate:bundle

    # Add into /app/pim/app/config.yml
    
    pimcore:
    
        bundles:
            search_paths:
                - vendor/kitt3n/
