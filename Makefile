ARGS = $(filter-out $@,$(MAKECMDGOALS))
MAKEFLAGS += --silent

list:
	sh -c "echo; $(MAKE) -p no_targets__ | awk -F':' '/^[a-zA-Z0-9][^\$$#\/\\t=]*:([^=]|$$)/ {split(\$$1,A,/ /);for(i in A)print A[i]}' | grep -v '__\$$' | grep -v 'Makefile'| sort"

#############################
# Build PIM
#############################

dev-build:
	sh ./docker-dev-build.sh

dev-rebuild:
	sh ./docker-dev-rebuild.sh

dev-up:
	sh ./docker-dev-up.sh

dev-stop:
	sh ./docker-dev-stop.sh

dev-root:
	sh ./make.dev.root.sh

dev-gulp:
	sh ./make.dev.gulp.sh

dev-apache:
	sh ./make.dev.apache.sh


start-on-linux:
	make dev-up

stop-on-linux:
	make dev-stop

root-on-linux:
	make dev-root

gulp-on-linux:
	make dev-gulp

apache-on-linux:
	make dev-apache


pre-build:
	sh ./docker-pre-build.sh

pre-up:
	sh ./docker-pre-up.sh

pre-down:
	sh ./docker-pre-down.sh

pre-down-volumes:
	sh ./docker-pre-down-volumes.sh

pre-stop:
	sh ./docker-pre-stop.sh

pre-root:
	sh ./make.pre.root.sh

pre-apache:
	sh ./make.pre.apache.sh


build:
	sh ./docker-build.sh

up:
	sh ./docker-up.sh

down:
	sh ./docker-down.sh

down-volumes:
	sh ./docker-down-volumes.sh

down-last-build:
	sh ./docker-down-last-build.sh

stop:
	sh ./docker-stop.sh

push:
	sh ./docker-push.sh

pull:
	sh ./docker-pull.sh

root:
	sh ./make.root.sh

apache:
	sh ./make.apache.sh

#mysql-import-pim:
#	sh ./make.mysql-import-pim.sh

#############################
# Argument fix workaround
#############################
%:
	@:
