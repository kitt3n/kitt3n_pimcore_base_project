# Prepare deployment to hetzner cloud server

Put keys into ``.ssh`` (once).

Prepare

* .gitignore (once via ``$ sh 0_prepate-gitignore.sh``)
* .env

## From local (development)
  
Copy assets into ``mount/<x>/app/pim/web/var/assets``

Copy versions into ``mount/<x>/app/pim/var/versions``

    $ mkdir -p mount/1/app/pim/web/var
    $ docker cp -a <appcontainer>:/app/pim/web/var/assets ./mount/1//app/pim/web/var/assets
    
    $ mkdir -p mount/1/app/pim/var 
    $ docker cp -a <appcontainer>:/app/pim/var/versions ./mount/1/app/pim/var/versions

Copy sql dump from clocal development mysql container ``<x-1`` into ``mysql_seed_pre/<x>``. Keep ``a.sql``!

Go to ``1.1``.

## Existing (production) deployment

### Prepare build x

Copy sql dump from current build ``<x-1>`` into ``mysql_seed_pre/<x>``. Keep ``a.sql``!

#### 1 Prepare prebuild

Run

    $ sh 1_prepare-pre-build.sh

This will download ``assets`` and ``versions`` from current build ``<x-1>``.

##### 1.1 Build prebuild

    $ TMPDIR=$(pwd) make pre-build
    $ make pre-up

Make changes to the pre build if needed.

#### 2 Prepare build

Run 

    $ sh 2_prepare-build.sh

This will copy ``assets`` and ``versions`` from pre built docker container to ``./copy/build`` and a database dump to ``./mysql_seed/<x>``.

##### 2.1 Build

    $ make pre-down
    $ TMPDIR=$(pwd) make build

##### 2.2 Push

    $ docker login
    $ make push

#### 3 Prepare deployment

On server (clone) pull branch build/<x> and checkout build/<x>

Run

    $ 3_prepare-deployment.sh

This will upload ``assets``, ``versions`` and ``sql dump``.

##### 3.1 Run build

###### 3.1.1 Run traefik proxy

    # Once
    $ docker network create traefik

    $ cd ./traefik && chmod 600 ./acme.json && docker-compose up -d

###### 3.1.2 Run PIM

    # Once
    $ docker login

    # First stop current build!
    
    $ cd .. && make up

> Don't forget to disable product pages in TYPO3 before stopping ``<x-1>`` and starting ``<x>``

> You need to checkout ``build/<x-1>`` to stop current build.
