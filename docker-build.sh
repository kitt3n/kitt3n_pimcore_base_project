#!/usr/bin/env bash
export $(cat .env | grep -v ^# | xargs)
echo "--project-name ${BUILD_PROJECT_NAME}_${BUILD_VERSION}"
read -r -p "This will REMOVE any previously built CONTAINERS and VOLUMES. Continue? [Y/n] " response
case "$response" in
    [yY][eE][sS]|[yY])
        docker-compose -f ./docker-compose.build.yaml --project-name "${BUILD_PROJECT_NAME}_${BUILD_VERSION}" down --volumes
        docker-compose -f ./docker-compose.build.yaml --project-name "${BUILD_PROJECT_NAME}_${BUILD_VERSION}" rm --force &&
        docker-compose -f ./docker-compose.build.yaml --project-name "${BUILD_PROJECT_NAME}_${BUILD_VERSION}" build --no-cache
        ;;
    *)
        echo "Exit without building."
        ;;
esac

