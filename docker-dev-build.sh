#!/usr/bin/env bash
export $(cat .env | grep -v ^# | xargs)
echo "--project-name ${DEV_PREFIX}_${BUILD_PROJECT_NAME}_dev"
read -r -p "This will REMOVE any previously built CONTAINERS and VOLUMES. Continue? [y/N] " response
case "$response" in
    [yY][eE][sS]|[yY])
        docker-compose -f ./docker-compose.dev.yaml --project-name "${DEV_PREFIX}_${BUILD_PROJECT_NAME}_dev" down --volumes
        docker-compose -f ./docker-compose.dev.yaml --project-name "${DEV_PREFIX}_${BUILD_PROJECT_NAME}_dev" rm --force &&
        docker-compose -f ./docker-compose.dev.yaml --project-name "${DEV_PREFIX}_${BUILD_PROJECT_NAME}_dev" build --no-cache
        ;;
    *)
        echo "Exit without building."
        ;;
esac

