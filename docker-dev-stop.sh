#!/usr/bin/env bash
export $(cat .env | grep -v ^# | xargs)
echo "--project-name ${DEV_PREFIX}_${BUILD_PROJECT_NAME}_dev"
docker-compose -f ./docker-compose.dev.yaml --project-name "${DEV_PREFIX}_${BUILD_PROJECT_NAME}_dev" stop