#!/usr/bin/env bash
export $(cat .env | grep -v ^# | xargs)
echo "--project-name ${BUILD_PROJECT_NAME}_${BUILD_VERSION}"

LAST_BUILD_VERSION=0
let LAST_BUILD_VERSION=${BUILD_VERSION}-1

echo "LAST_BUILD_VERSION: ${LAST_BUILD_VERSION}"

read -r -p "Continue? [Y/n] " response
case "$response" in
    [yY][eE][sS]|[yY])

        echo "Continue."

        docker-compose -f ./docker-compose.run.yaml --project-name "${BUILD_PROJECT_NAME}_${LAST_BUILD_VERSION}" down
        ;;
    *)
        echo "Exit."
        ;;
esac