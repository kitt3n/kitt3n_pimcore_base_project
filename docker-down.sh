#!/usr/bin/env bash
export $(cat .env | grep -v ^# | xargs)
echo "--project-name ${BUILD_PROJECT_NAME}_${BUILD_VERSION}"
docker-compose -f ./docker-compose.run.yaml --project-name "${BUILD_PROJECT_NAME}_${BUILD_VERSION}" down