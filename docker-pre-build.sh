#!/usr/bin/env bash
export $(cat .env | grep -v ^# | xargs)
echo "--project-name ${BUILD_PROJECT_NAME}_pre_${BUILD_VERSION}"
read -r -p "This will REMOVE any previously built CONTAINERS and VOLUMES. Continue? [y/N] " response
case "$response" in
    [yY][eE][sS]|[yY])
        docker-compose -f ./docker-compose.pre.build.yaml --project-name "${BUILD_PROJECT_NAME}_pre_${BUILD_VERSION}" down --volumes
        docker-compose -f ./docker-compose.pre.build.yaml --project-name "${BUILD_PROJECT_NAME}_pre_${BUILD_VERSION}" rm --force &&
        docker-compose -f ./docker-compose.pre.build.yaml --project-name "${BUILD_PROJECT_NAME}_pre_${BUILD_VERSION}" build --no-cache
        ;;
    *)
        echo "Exit without building."
        ;;
esac

