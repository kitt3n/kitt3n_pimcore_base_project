#!/usr/bin/env bash
export $(cat .env | grep -v ^# | xargs)
echo "--project-name ${BUILD_PROJECT_NAME}_pre_${BUILD_VERSION} down --volumes"
read -r -p "This will REMOVE any previously built CONTAINERS and VOLUMES. Continue? [y/N] " response
case "$response" in
    [yY][eE][sS]|[yY])
        docker-compose -f ./docker-compose.pre.run.yaml --project-name "${BUILD_PROJECT_NAME}_pre_${BUILD_VERSION}" down --volumes
        ;;
    *)
        echo "Exit without removing anything."
        ;;
esac
