#!/usr/bin/env bash
export $(cat .env | grep -v ^# | xargs)
echo "--project-name ${BUILD_PROJECT_NAME}_pre_${BUILD_VERSION}"
docker-compose -f ./docker-compose.pre.run.yaml --project-name "${BUILD_PROJECT_NAME}_pre_${BUILD_VERSION}" up -d

echo "Initialize"
echo "Sleep 1m"
sleep 1m
echo "docker exec..."

docker exec -it -u root $(docker-compose -f ./docker-compose.pre.run.yaml --project-name "${BUILD_PROJECT_NAME}_pre_${BUILD_VERSION}" ps -q app) /bin/zsh -c "mysql -V && cd /app/pim/ && php bin/console debug:config doctrine && php bin/console pimcore:bundle:list && php bin/console pimcore:bundle:enable Kitt3nPimcoreRestrictionsBundle && php bin/console pimcore:bundle:uninstall Kitt3nPimcoreRestrictionsBundle && php bin/console pimcore:bundle:install Kitt3nPimcoreRestrictionsBundle && echo '(Re)build classes' && chmod -R 777 /app/pim/web/var && chmod -R 777 /app/pim/var && php bin/console pimcore:deployment:classes-rebuild && echo 'Clear caches' && php bin/console pimcore:cache:clear && echo 'Warm caches' && php bin/console pimcore:cache:warming --env=prod && php bin/console assets:install /app/pim/web && php bin/console pimcore:thumbnails:image && chmod -R 777 /app/pim/web/var && chmod -R 777 /app/pim/var"