#!/usr/bin/env bash
export $(cat .env | grep -v ^# | xargs)
echo "--project-name ${BUILD_PROJECT_NAME}_${BUILD_VERSION}"
docker-compose -f ./docker-compose.run.yaml --project-name "${BUILD_PROJECT_NAME}_${BUILD_VERSION}" up -d

read -r -p "Reiniatialize? [y/N] " response
case "$response" in
    [yY][eE][sS]|[yY])
        echo "Initialize"
        echo "Sleep 1m"
        sleep 1m
        echo "docker exec..."

        docker exec -it -u root $(docker-compose -f ./docker-compose.run.yaml --project-name "${BUILD_PROJECT_NAME}_${BUILD_VERSION}" ps -q app) /bin/zsh -c "mysql -V && cd /app/pim/ && php bin/console debug:config doctrine && php bin/console pimcore:bundle:list && php bin/console pimcore:bundle:enable Kitt3nPimcoreRestrictionsBundle && php bin/console pimcore:bundle:uninstall Kitt3nPimcoreRestrictionsBundle && php bin/console pimcore:bundle:install Kitt3nPimcoreRestrictionsBundle && echo '(Re)build classes' && chmod -R 777 /app/pim/web/var && chmod -R 777 /app/pim/var && php bin/console pimcore:deployment:classes-rebuild && echo 'Clear caches' && php bin/console pimcore:cache:clear && echo 'Warm caches' && php bin/console pimcore:cache:warming --env=prod && php bin/console assets:install /app/pim/web && php bin/console pimcore:thumbnails:image && chmod -R 777 /app/pim/web/var && chmod -R 777 /app/pim/var"
        ;;
    *)
        echo "Not reinitializing."
        ;;
esac

#docker exec -it -u root $(docker-compose -f ./docker-compose.run.yaml --project-name "${BUILD_PROJECT_NAME}_${BUILD_VERSION}" ps -q app) /bin/zsh -c 'mysql -V && echo "Sleep 1m" && sleep 1m && cd /app/pim/ && php bin/console debug:config doctrine && echo "(Re)build classes" && php bin/console pimcore:deployment:classes-rebuild && echo "Sleep 2m" && sleep 2m && echo "Warm caches" && php bin/console pimcore:cache:warming --env=prod'

#"cd /app/pim/ && php bin/console pimcore:deployment:classes-rebuild && sleep 2m && php bin/console pimcore:cache:warming --env=prod"