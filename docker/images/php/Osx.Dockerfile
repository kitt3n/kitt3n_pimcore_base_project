#+++++++++++++++++++++++++++++++++++++++
# Dockerfile for kitt3n/kitt3n_php:7.3
#+++++++++++++++++++++++++++++++++++++++

FROM kitt3n/kitt3n_php:7.3

# link xdebug config for osx
RUN ln -sf /opt/docker/etc/php/xdebugfordockerformac.ini /usr/local/etc/php/conf.d/xdebugfordockerformac.ini