#!/usr/bin/env bash
export $(cat .env | grep -v ^# | xargs)
docker exec -it -u root $(docker-compose -f ./docker-compose.run.yaml --project-name "${BUILD_PROJECT_NAME}_${BUILD_VERSION}" ps -q app) /bin/zsh